# REIGN FULL STACK TEST

This test consist of a server made using [NestJS](https://nestjs.com/) framework and a client using [React](https://reactjs.org/) library

### The System

```
OS: Manjaro Linux x86_64
Kernel: 5.8.16-2-MANJARO
Shell: bash 5.0.18

$ npm -v 6.14.7
$ yarn -v 1.22.5
$ node -v v14.12.0
$ nest -v 7.5.1
$ docker -v Docker version 19.03.12-ce, build 48a66213fe
```

### Tools

- [Visual Studio Code](https://code.visualstudio.com/) : IDE
- [Insomnia](https://insomnia.rest/) : Desktop API Client.
- [MongoDB Compass](https://www.mongodb.com/products/compass) : DB GUI
- [Bash Terminal](https://www.gnu.org/software/bash/) : Linux Terminal
- [Docker](https://www.docker.com/) : Container

### Directory Structure

This repository contains two folders one for the server and one for the client

```
.
├── reign-client [React App]
└── reign-server [NestJS Server]
```

----------

## Client

The client is a React app that was created using the create-react-app command then modified as required

```
$ npx create-react-app reign-client
```

In adition of the default packages I added two more dependencies to the project

- [Axios](https://github.com/axios/axios) : HttpClient instead of React fetch
- [Moment.js](https://momentjs.com/) : Used to manipulate/format dates

```
$ yarn add axios moment
```

----------

## Server

The server is a NestJs server that was created using the CLI that has to be installed

```
$ npm i -g @nestjs/cli
$ nest new reign-server
```


In adition of the default packages I added a couple more dependencies to the project

- [Mongoose](https://docs.nestjs.com/techniques/mongodb) : MongoDB library
- [Task Scheduling](https://docs.nestjs.com/techniques/task-scheduling) : Cron like funtionality

```
$ yarn add mongoose @nestjs/mongoose @nestjs/schedule
```

### Server API

#### API Endpoints
```
GET /articles/
Params N/A
Response Articles[]

- Description: Query the DB Collection to obtain a list of articles

- Used for: Obtain list of articles to display in the React App 
```

```
PUT /articles/
Params N/A
Response Articles[]

- Description: Clears the collection and then ask makes a request to the API https://hn.algolia.com/ and then the hits are saved in the DB and then returns the hits list

- Used for: Cron Tasks in the server that runs every hour
```

```
DELETE /articles/{ArticleID}
Params ArticleID from DB
Response N/A

- Description: Deletes an article from the DB collection

- Used for: Delete button functionalitiy in the React App
```


# INSTALLATION INSTRUCTIONS

1. DB Docker
2. Server
3. Client

## DB DOCKER
----------

This project uses MongoDB Docker using the file [Docker Compose File](./docker-compose.yml)


Spinup Docker

```
$ sudo docker-compose up -d
```

## SERVER
----------

This project uses [DockerFile](./reign-server/Dockerfile)

Edit [App Module](./reign-server/src/app.module.ts) code line 12 and change to your local IP (TODO: use dotenv)
```
'mongodb://root:123456789@<YOUR-LOCAL-IP-HERE>:27017/reign?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=false',
```


Spinup Docker

```
$ sudo docker build -t reign-server:dev .
```

```
$ sudo docker run -it \
                  --rm -v ${PWD}:/app \
                  -v /app/node_modules -p 8080:8080 \
                  -e CHOKIDAR_USEPOLLING=true \
                  reign-server:dev
```

Server Started Log
```
[Nest] 1   - 10/21/2020, 2:15:07 AM   [NestFactory] Starting Nest application...
[Nest] 1   - 10/21/2020, 2:15:07 AM   [InstanceLoader] MongooseModule dependencies initialized +29ms
[Nest] 1   - 10/21/2020, 2:15:07 AM   [InstanceLoader] HttpModule dependencies initialized +1ms
[Nest] 1   - 10/21/2020, 2:15:07 AM   [InstanceLoader] DiscoveryModule dependencies initialized +0ms
[Nest] 1   - 10/21/2020, 2:15:07 AM   [InstanceLoader] AppModule dependencies initialized +0ms
[Nest] 1   - 10/21/2020, 2:15:07 AM   [InstanceLoader] TasksModule dependencies initialized +1ms
[Nest] 1   - 10/21/2020, 2:15:07 AM   [InstanceLoader] ScheduleModule dependencies initialized +0ms
[Nest] 1   - 10/21/2020, 2:15:07 AM   [InstanceLoader] MongooseCoreModule dependencies initialized +30ms
[Nest] 1   - 10/21/2020, 2:15:07 AM   [InstanceLoader] MongooseModule dependencies initialized +9ms
[Nest] 1   - 10/21/2020, 2:15:07 AM   [InstanceLoader] ArticlesModule dependencies initialized +1ms
[Nest] 1   - 10/21/2020, 2:15:07 AM   [RoutesResolver] AppController {}: +5ms
[Nest] 1   - 10/21/2020, 2:15:07 AM   [RouterExplorer] Mapped {, GET} route +3ms
[Nest] 1   - 10/21/2020, 2:15:07 AM   [RoutesResolver] ArticlesController {/articles}: +1ms
[Nest] 1   - 10/21/2020, 2:15:07 AM   [RouterExplorer] Mapped {/articles, GET} route +0ms
[Nest] 1   - 10/21/2020, 2:15:07 AM   [RouterExplorer] Mapped {/articles/:id, GET} route +1ms
[Nest] 1   - 10/21/2020, 2:15:07 AM   [RouterExplorer] Mapped {/articles, POST} route +1ms
[Nest] 1   - 10/21/2020, 2:15:07 AM   [RouterExplorer] Mapped {/articles, PUT} route +0ms
[Nest] 1   - 10/21/2020, 2:15:07 AM   [RouterExplorer] Mapped {/articles/:id, DELETE} route +1ms
[Nest] 1   - 10/21/2020, 2:15:07 AM   [NestApplication] Nest application successfully started +15ms
```

Populate DB

```
$ curl --request PUT \
  --url http://localhost:8080/articles
```

## CLIENT
----------

This project uses [DockerFile](./reign-client/Dockerfile)

In a new terminal Spinup Docker

```
$ sudo docker build -t reign-client:dev .
```

```
$ sudo docker run -it \
                  --rm -v ${PWD}:/app \
                  -v /app/node_modules -p 3001:3000 \
                  -e CHOKIDAR_USEPOLLING=true \
                  reign-client:dev
```
Client Started Log            

```
Compiled successfully!

You can now view reign-client in the browser.

  Local:            http://localhost:3000
  On Your Network:  http://172.17.0.3:3000

Note that the development build is not optimized.
To create a production build, use yarn build.
```

Open App in Browser

![screenshot](./reign-client/screenshot.png)