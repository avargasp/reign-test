import { HttpModule } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { ArticlesService } from './articles.service';
import { Article, ArticleSchema } from './schemas/article.schema';

describe('ArticlesService', () => {
  let service: ArticlesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        MongooseModule.forFeature([{ name: 'Article', schema: ArticleSchema }]),
        HttpModule,
        MongooseModule.forRoot(
          'mongodb://root:123456789@localhost:27017/reign?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=false',
          { useFindAndModify: false },
        ),
      ],
      providers: [ArticlesService],
    }).compile();

    service = module.get<ArticlesService>(ArticlesService);
  });

  it('Should return an array with one element', async () => {
    const result = [
      {
        created_at: '',
        title: '',
        url: '',
        story_url: '',
        story_title: '',
        author: '',
      },
    ] as Article[];
    jest
      .spyOn(service, 'obtainArticles')
      .mockImplementation(async () => result);

    expect((await service.obtainArticles()).length).toBe(1);
  });

  it('Checks all of the services functions', async () => {
    const result = await service.obtainArticles();
    let dbArticles = await service.findAll();

    expect(dbArticles.length).toBe(result.length);

    const articleId = (result[0] as any)._id;
    await service.delete(articleId);
    dbArticles = await service.findAll();

    expect(dbArticles.length).toBe(result.length - 1);

    const dummyArticle = {
      createdAt: '2020-10-20T02:55:29.000Z',
      title: null,
      url: null,
      author: 'dgentile',
      storyTitle: 'Free Software Is More Reliable',
      storyUrl: 'https://www.gnu.org/software/reliability.en.html',
    };

    await service.create(dummyArticle);
    dbArticles = await service.findAll();

    expect(
      dbArticles.find(article => article.author === dummyArticle.author),
    ).not.toBeUndefined();
  });
});
