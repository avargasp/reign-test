export class CreateArticleDto {
  createdAt: string;
  title: string;
  url: string;
  author: string;
  storyTitle: string;
  storyUrl: string;
}
