import { CreateArticleDto } from './create-article.dto';

describe('CreateArticleDto', () => {
  it('should be defined', () => {
    expect(new CreateArticleDto()).toBeDefined();
  });
});
