export interface Article {
  'created_at': string;
  title: string;
  url: string;
  author: string;
  'story_title': string;
  'story_url': string;
}