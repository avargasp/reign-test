import {
  Controller,
  Get,
  Query,
  Post,
  Body,
  Put,
  Param,
  Delete,
} from '@nestjs/common';
import { CreateArticleDto } from './dto/create-article.dto';
import { ArticlesService } from './articles.service';
import { Article } from './interfaces/article.interface';

@Controller('articles')
export class ArticlesController {
  constructor(private articlesService: ArticlesService) {}

  @Get()
  async findAll(): Promise<Article[]> {
    return this.articlesService.findAll();
  }

  @Get(':id')
  async find(@Param('id') id: string) {
    return this.articlesService.find(id);
  }

  @Post()
  async create(@Body() createArticleDto: CreateArticleDto) {
    this.articlesService.create(createArticleDto);
  }

  @Put()
  async loadArticles(): Promise<any> {
    return this.articlesService.obtainArticles();
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    this.articlesService.delete(id);
  }
}
