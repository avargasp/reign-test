import { Model } from 'mongoose';
import { HttpService, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Article, ArticleDocument } from './schemas/article.schema';
import { CreateArticleDto } from './dto/create-article.dto';

@Injectable()
export class ArticlesService {
  constructor(
    @InjectModel('Article') private articleModel: Model<ArticleDocument>,
    private readonly httpService: HttpService,
  ) {}

  async create(createArticleDto: CreateArticleDto): Promise<Article> {
    const createdArticle = new this.articleModel(createArticleDto);
    return createdArticle.save();
  }

  async obtainArticles(): Promise<Article[]> {
    this.articleModel.collection.drop();

    const response = await this.httpService
      .get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
      .toPromise();

    for await (const article of response.data.hits) {
      const createdArticle = new this.articleModel(article);
      await createdArticle.save();
    }

    return this.articleModel.find().exec();
  }

  async findAll(): Promise<Article[]> {
    return this.articleModel
      .find()
      .sort('-created_at')
      .exec();
  }

  async find(id: string): Promise<Article> {
    return this.articleModel.findOne({ _id: id });
  }

  async delete(id: string): Promise<Article> {
    return this.articleModel.findByIdAndRemove(id);
  }
}
