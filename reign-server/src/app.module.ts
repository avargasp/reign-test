import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ArticlesModule } from './articles/articles.module';
import { TasksModule } from './tasks/tasks.module';

@Module({
  imports: [
    MongooseModule.forRoot(
      'mongodb://root:123456789@192.168.0.4:27017/reign?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=false',
      { useFindAndModify: false },
    ),
    ScheduleModule.forRoot(),
    ArticlesModule,
    TasksModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
