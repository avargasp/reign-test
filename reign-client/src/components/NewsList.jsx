import axios from "axios";
import React, { useEffect, useState } from "react";
import "../styles/App.css";
import NewsListItem from "./NewsListItem";

const NewsItemList = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [feedData, setFeedData] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);

      axios
        .get("http://localhost:8080/articles")
        .then(response => {
          if (response.data) setFeedData(response.data);
        })
        .catch(error => {
          // handle error
          console.log(error);
        })
        .then(() => {
          setIsLoading(false);
        });
    };

    fetchData();
  }, []);

  const onItemDeleted = id => {
    const filteredFeed = feedData.filter(entry => entry._id !== id);
    setFeedData(filteredFeed);

    axios
      .delete("http://localhost:8080/articles/" + id)
      .then(() => {})
      .catch(error => {
        // handle error
        console.log(error);
      });
  };

  return isLoading ? (
    <div className="NewsLoading">Loading...</div>
  ) : (
    <ul className="NewsList">
      {feedData && feedData.length > 0
        ? feedData
            .filter(entry => entry.story_title || entry.title)
            .map(entry => (
              <NewsListItem
                key={entry._id}
                data={entry}
                onItemDeleted={() => onItemDeleted(entry._id)}
              />
            ))
        : "No entries"}
    </ul>
  );
};

export default NewsItemList;
