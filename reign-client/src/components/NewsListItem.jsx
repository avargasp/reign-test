import moment from "moment";
import React, { useEffect, useState } from "react";
import "../styles/App.css";

const NewsListItem = ({ data, onItemDeleted }) => {
  const [title, setTitle] = useState(null);
  const [author, setAuthor] = useState(null);
  const [date, setDate] = useState(null);
  const [url, setUrl] = useState(null);
  const [showDelete, setShowDelete] = useState(false);

  useEffect(() => {
    const { story_title, title, author, created_at, story_url, url } = data;

    if (story_title) setTitle(story_title);
    else if (title) setTitle(title);

    setAuthor(author);

    const entryDate = moment(created_at);
    const currentDate = moment();

    if (currentDate.isSame(entryDate, "day")) setDate(entryDate.format("h:mm a"));
    else if (currentDate.subtract(1, "days").date() === entryDate.date()) setDate("Yesterday");
    else if (currentDate.isSame(entryDate, "year")) setDate(entryDate.format("MMM DD"));
    else setDate(entryDate.format("MM/DD/YYYY"));

    if (story_url) setUrl(story_url);
    else if (url) setUrl(url);
  }, [data]);

  return (
    <li
      className="NewsListItem"
      onClick={e => {
        window.open(url, "_blank");
      }}
      onMouseEnter={() => setShowDelete(true)}
      onMouseLeave={() => setShowDelete(false)}
    >
      <div className="NewsListItemTitle">
        {title}
        <span>- {author} -</span>
      </div>
      <div className="NewsListItemDate">{date}</div>
      <div className="NewsListItemButton">
        {showDelete && (
          <i
            className="fa fa-trash"
            onClick={e => {
              onItemDeleted();
              e.stopPropagation();
            }}
          />
        )}
      </div>
    </li>
  );
};

export default NewsListItem;
