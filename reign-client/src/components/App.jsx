import React from "react";
import "../styles/App.css";
import NewsList from "./NewsList";

const App = () => {
  return (
    <div className="App">
      <header className="App-header">
        <h1>HN Feed</h1>
        <h2>We &lt;3 hacker news!</h2>
      </header>
      <NewsList />
    </div>
  );
};

export default App;
